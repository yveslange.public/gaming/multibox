#!/usr/bin/env node
// This script will build the AHK script for multiboxing.

const fs = require('fs');

const configFilename = './config.js';

let CONFIG = null;
try {
  CONFIG = require(configFilename);
} catch (e) {
  console.log('Missing configuration file, copying the sample and using it.');
  fs.copyFileSync('./config.sample.js', configFilename);
  console.log(
    'Copying the config.sample.js, please restart the application manually',
  );
  process.exit(1);
}

/**
 * AHK Reminders
 * - modifiers: #=WIN, ^=CTRL, !=ALT, +=SHIFT
 * - prefixes: ~=FIRES_WHEN_KEYPRESSED
 */

/**
 * Multibox class that will generate the AHK script
 */
class Multibox {
  constructor() {
    this.windowName = CONFIG.windowName;
    this.numberOfWindow = CONFIG.numberOfWindow;
    this.SCRIPT = this.initScript();
    this.initScript();
  }

  /**
   * Initiaze the AHK Script with the PAUSE hotkey to suspend and a
   * notification to show when the script is on or off.
   */
  initScript() {
    return `#SingleInstance force

WinGet, windowid, List, ${this.windowName}
; SUSPEND KEYS will suspend the use of all other hotkeys in this script
~Pause::
Suspend, permit
if (State = 0) {
  Gui, +AlwaysOnTop +Disabled -SysMenu +Owner
  Gui, Font, s28, Arial
  Gui, Add, Text, cGreen, On
  Gui, Show, xCenter y10, State, NoActivate,
  sleep, 800
  Gui, Destroy
  State++
}
else {
  State := 0
  Gui, +AlwaysOnTop +Disabled -SysMenu +Owner
  Gui, Font, s28, Arial
  Gui, Add, Text, cRed, Off
  Gui, Show, xCenter y10, State, NoActivate,
  sleep, 800
  Gui, Destroy
}
Suspend, toggle
return
`;
  }

  /**
   * Add a hotkey to the AHK script
   * @param {String} key - A key to bind.
   * @param {Array} modifiers - A list of modifier ''(empty string), ^, !, +.
   * Use an empty string to bind the key without a modifier.
   * @param {String} prefix - The prefix to add in AHK before the key (mainly ~)
   */
  addHotkey(key, modifiers = ['', '^', '!', '+'], prefix = '~') {
    // If the modifiers is not set, add the key without any modifier.
    if (!modifiers || modifiers.length === 0) {
      modifiers.push('');
    }

    const MOD_NAMES = {
      '^': 'Ctrl',
      '!': 'Alt',
      '+': 'Shift',
    };

    // Char to redefined
    const KEY_REF = {
      '§': 'sc029',
    };

    const KEY_UC = KEY_REF[key] ? KEY_REF[key] : key.toUpperCase();

    modifiers.forEach((mod) => {
      // Add a check to see if the window is active
      this.SCRIPT += `\n#IfWinActive, ${this.windowName}`;
      this.SCRIPT += `\n${prefix}${mod}${KEY_UC}::`;

      // Add hotkey for all windows.
      for (
        let winNumber = 1;
        winNumber < this.numberOfWindow + 1;
        winNumber += 1
      ) {
        let modDown = '';
        let modUp = '';
        if (mod) {
          const modName = MOD_NAMES[mod];
          modDown = `{${modName} down}`;
          modUp = `{${modName} up}`;
        }
        this.SCRIPT += `\nControlSend,,${modDown}{${KEY_UC} down}{${KEY_UC} up}${modUp}, ahk_id %windowid${winNumber}%`;
      }

      // Adds the return close
      this.SCRIPT += '\nreturn\n';
    });
  }

  /**
   * Write the file
   * @param {String} filename
   */
  writeFile(filename) {
    const { SCRIPT } = this;
    fs.writeFile(filename, SCRIPT, 'utf8', (err) => {
      if (err) {
        console.log(err);
        return false;
      }
      console.log(`File written to ${filename} (${SCRIPT.length} characters)`);
      return true;
    });
  }
}

// Let's start.
const multibox = new Multibox();
let NUMBEROFKEYS = 0;
CONFIG.keys.forEach((key) => {
  if (CONFIG.ignoreKeys.indexOf(key) === -1) {
    NUMBEROFKEYS += 1;
    if (typeof key === 'object') {
      multibox.addHotkey(key.code, key.modifiers, key.prefix);
    } else {
      multibox.addHotkey(key);
    }
  }
});
multibox.writeFile('script.ahk');
console.log(`Done. ${NUMBEROFKEYS} keys configured.`);
