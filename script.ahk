#SingleInstance force

WinGet, windowid, List, World of Warcraft
; SUSPEND KEYS will suspend the use of all other hotkeys in this script
~Pause::
Suspend, permit
if (State = 0) {
  Gui, +AlwaysOnTop +Disabled -SysMenu +Owner
  Gui, Font, s28, Arial
  Gui, Add, Text, cGreen, On
  Gui, Show, xCenter y10, State, NoActivate,
  sleep, 800
  Gui, Destroy
  State++
}
else {
  State := 0
  Gui, +AlwaysOnTop +Disabled -SysMenu +Owner
  Gui, Font, s28, Arial
  Gui, Add, Text, cRed, Off
  Gui, Show, xCenter y10, State, NoActivate,
  sleep, 800
  Gui, Destroy
}
Suspend, toggle
return

#IfWinActive, World of Warcraft
~1::
ControlSend,,{1 down}{1 up}, ahk_id %windowid1%
ControlSend,,{1 down}{1 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^1::
ControlSend,,{Ctrl down}{1 down}{1 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{1 down}{1 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!1::
ControlSend,,{Alt down}{1 down}{1 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{1 down}{1 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+1::
ControlSend,,{Shift down}{1 down}{1 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{1 down}{1 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~2::
ControlSend,,{2 down}{2 up}, ahk_id %windowid1%
ControlSend,,{2 down}{2 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^2::
ControlSend,,{Ctrl down}{2 down}{2 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{2 down}{2 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!2::
ControlSend,,{Alt down}{2 down}{2 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{2 down}{2 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+2::
ControlSend,,{Shift down}{2 down}{2 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{2 down}{2 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~3::
ControlSend,,{3 down}{3 up}, ahk_id %windowid1%
ControlSend,,{3 down}{3 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^3::
ControlSend,,{Ctrl down}{3 down}{3 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{3 down}{3 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!3::
ControlSend,,{Alt down}{3 down}{3 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{3 down}{3 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+3::
ControlSend,,{Shift down}{3 down}{3 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{3 down}{3 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~4::
ControlSend,,{4 down}{4 up}, ahk_id %windowid1%
ControlSend,,{4 down}{4 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^4::
ControlSend,,{Ctrl down}{4 down}{4 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{4 down}{4 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!4::
ControlSend,,{Alt down}{4 down}{4 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{4 down}{4 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+4::
ControlSend,,{Shift down}{4 down}{4 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{4 down}{4 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~5::
ControlSend,,{5 down}{5 up}, ahk_id %windowid1%
ControlSend,,{5 down}{5 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^5::
ControlSend,,{Ctrl down}{5 down}{5 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{5 down}{5 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!5::
ControlSend,,{Alt down}{5 down}{5 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{5 down}{5 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+5::
ControlSend,,{Shift down}{5 down}{5 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{5 down}{5 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~6::
ControlSend,,{6 down}{6 up}, ahk_id %windowid1%
ControlSend,,{6 down}{6 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^6::
ControlSend,,{Ctrl down}{6 down}{6 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{6 down}{6 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!6::
ControlSend,,{Alt down}{6 down}{6 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{6 down}{6 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+6::
ControlSend,,{Shift down}{6 down}{6 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{6 down}{6 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~7::
ControlSend,,{7 down}{7 up}, ahk_id %windowid1%
ControlSend,,{7 down}{7 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^7::
ControlSend,,{Ctrl down}{7 down}{7 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{7 down}{7 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!7::
ControlSend,,{Alt down}{7 down}{7 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{7 down}{7 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+7::
ControlSend,,{Shift down}{7 down}{7 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{7 down}{7 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~8::
ControlSend,,{8 down}{8 up}, ahk_id %windowid1%
ControlSend,,{8 down}{8 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^8::
ControlSend,,{Ctrl down}{8 down}{8 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{8 down}{8 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!8::
ControlSend,,{Alt down}{8 down}{8 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{8 down}{8 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+8::
ControlSend,,{Shift down}{8 down}{8 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{8 down}{8 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~9::
ControlSend,,{9 down}{9 up}, ahk_id %windowid1%
ControlSend,,{9 down}{9 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^9::
ControlSend,,{Ctrl down}{9 down}{9 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{9 down}{9 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!9::
ControlSend,,{Alt down}{9 down}{9 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{9 down}{9 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+9::
ControlSend,,{Shift down}{9 down}{9 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{9 down}{9 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~0::
ControlSend,,{0 down}{0 up}, ahk_id %windowid1%
ControlSend,,{0 down}{0 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^0::
ControlSend,,{Ctrl down}{0 down}{0 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{0 down}{0 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!0::
ControlSend,,{Alt down}{0 down}{0 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{0 down}{0 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+0::
ControlSend,,{Shift down}{0 down}{0 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{0 down}{0 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F1::
ControlSend,,{F1 down}{F1 up}, ahk_id %windowid1%
ControlSend,,{F1 down}{F1 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F1::
ControlSend,,{Ctrl down}{F1 down}{F1 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F1 down}{F1 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F1::
ControlSend,,{Alt down}{F1 down}{F1 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F1 down}{F1 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F1::
ControlSend,,{Shift down}{F1 down}{F1 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F1 down}{F1 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F2::
ControlSend,,{F2 down}{F2 up}, ahk_id %windowid1%
ControlSend,,{F2 down}{F2 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F2::
ControlSend,,{Ctrl down}{F2 down}{F2 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F2 down}{F2 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F2::
ControlSend,,{Alt down}{F2 down}{F2 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F2 down}{F2 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F2::
ControlSend,,{Shift down}{F2 down}{F2 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F2 down}{F2 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F3::
ControlSend,,{F3 down}{F3 up}, ahk_id %windowid1%
ControlSend,,{F3 down}{F3 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F3::
ControlSend,,{Ctrl down}{F3 down}{F3 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F3 down}{F3 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F3::
ControlSend,,{Alt down}{F3 down}{F3 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F3 down}{F3 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F3::
ControlSend,,{Shift down}{F3 down}{F3 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F3 down}{F3 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F4::
ControlSend,,{F4 down}{F4 up}, ahk_id %windowid1%
ControlSend,,{F4 down}{F4 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F4::
ControlSend,,{Ctrl down}{F4 down}{F4 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F4 down}{F4 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F4::
ControlSend,,{Alt down}{F4 down}{F4 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F4 down}{F4 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F4::
ControlSend,,{Shift down}{F4 down}{F4 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F4 down}{F4 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F5::
ControlSend,,{F5 down}{F5 up}, ahk_id %windowid1%
ControlSend,,{F5 down}{F5 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F5::
ControlSend,,{Ctrl down}{F5 down}{F5 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F5 down}{F5 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F5::
ControlSend,,{Alt down}{F5 down}{F5 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F5 down}{F5 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F5::
ControlSend,,{Shift down}{F5 down}{F5 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F5 down}{F5 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F6::
ControlSend,,{F6 down}{F6 up}, ahk_id %windowid1%
ControlSend,,{F6 down}{F6 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F6::
ControlSend,,{Ctrl down}{F6 down}{F6 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F6 down}{F6 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F6::
ControlSend,,{Alt down}{F6 down}{F6 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F6 down}{F6 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F6::
ControlSend,,{Shift down}{F6 down}{F6 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F6 down}{F6 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F7::
ControlSend,,{F7 down}{F7 up}, ahk_id %windowid1%
ControlSend,,{F7 down}{F7 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F7::
ControlSend,,{Ctrl down}{F7 down}{F7 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F7 down}{F7 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F7::
ControlSend,,{Alt down}{F7 down}{F7 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F7 down}{F7 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F7::
ControlSend,,{Shift down}{F7 down}{F7 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F7 down}{F7 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F8::
ControlSend,,{F8 down}{F8 up}, ahk_id %windowid1%
ControlSend,,{F8 down}{F8 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F8::
ControlSend,,{Ctrl down}{F8 down}{F8 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F8 down}{F8 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F8::
ControlSend,,{Alt down}{F8 down}{F8 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F8 down}{F8 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F8::
ControlSend,,{Shift down}{F8 down}{F8 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F8 down}{F8 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F9::
ControlSend,,{F9 down}{F9 up}, ahk_id %windowid1%
ControlSend,,{F9 down}{F9 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F9::
ControlSend,,{Ctrl down}{F9 down}{F9 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F9 down}{F9 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F9::
ControlSend,,{Alt down}{F9 down}{F9 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F9 down}{F9 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F9::
ControlSend,,{Shift down}{F9 down}{F9 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F9 down}{F9 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F10::
ControlSend,,{F10 down}{F10 up}, ahk_id %windowid1%
ControlSend,,{F10 down}{F10 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F10::
ControlSend,,{Ctrl down}{F10 down}{F10 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F10 down}{F10 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F10::
ControlSend,,{Alt down}{F10 down}{F10 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F10 down}{F10 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F10::
ControlSend,,{Shift down}{F10 down}{F10 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F10 down}{F10 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F11::
ControlSend,,{F11 down}{F11 up}, ahk_id %windowid1%
ControlSend,,{F11 down}{F11 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F11::
ControlSend,,{Ctrl down}{F11 down}{F11 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F11 down}{F11 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F11::
ControlSend,,{Alt down}{F11 down}{F11 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F11 down}{F11 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F11::
ControlSend,,{Shift down}{F11 down}{F11 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F11 down}{F11 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F12::
ControlSend,,{F12 down}{F12 up}, ahk_id %windowid1%
ControlSend,,{F12 down}{F12 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F12::
ControlSend,,{Ctrl down}{F12 down}{F12 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F12 down}{F12 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F12::
ControlSend,,{Alt down}{F12 down}{F12 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F12 down}{F12 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F12::
ControlSend,,{Shift down}{F12 down}{F12 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F12 down}{F12 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~B::
ControlSend,,{B down}{B up}, ahk_id %windowid1%
ControlSend,,{B down}{B up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^B::
ControlSend,,{Ctrl down}{B down}{B up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{B down}{B up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!B::
ControlSend,,{Alt down}{B down}{B up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{B down}{B up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+B::
ControlSend,,{Shift down}{B down}{B up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{B down}{B up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~C::
ControlSend,,{C down}{C up}, ahk_id %windowid1%
ControlSend,,{C down}{C up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^C::
ControlSend,,{Ctrl down}{C down}{C up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{C down}{C up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!C::
ControlSend,,{Alt down}{C down}{C up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{C down}{C up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+C::
ControlSend,,{Shift down}{C down}{C up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{C down}{C up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~E::
ControlSend,,{E down}{E up}, ahk_id %windowid1%
ControlSend,,{E down}{E up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^E::
ControlSend,,{Ctrl down}{E down}{E up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{E down}{E up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!E::
ControlSend,,{Alt down}{E down}{E up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{E down}{E up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+E::
ControlSend,,{Shift down}{E down}{E up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{E down}{E up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~F::
ControlSend,,{F down}{F up}, ahk_id %windowid1%
ControlSend,,{F down}{F up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^F::
ControlSend,,{Ctrl down}{F down}{F up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{F down}{F up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!F::
ControlSend,,{Alt down}{F down}{F up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{F down}{F up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+F::
ControlSend,,{Shift down}{F down}{F up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{F down}{F up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~G::
ControlSend,,{G down}{G up}, ahk_id %windowid1%
ControlSend,,{G down}{G up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^G::
ControlSend,,{Ctrl down}{G down}{G up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{G down}{G up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!G::
ControlSend,,{Alt down}{G down}{G up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{G down}{G up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+G::
ControlSend,,{Shift down}{G down}{G up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{G down}{G up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~H::
ControlSend,,{H down}{H up}, ahk_id %windowid1%
ControlSend,,{H down}{H up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^H::
ControlSend,,{Ctrl down}{H down}{H up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{H down}{H up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!H::
ControlSend,,{Alt down}{H down}{H up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{H down}{H up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+H::
ControlSend,,{Shift down}{H down}{H up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{H down}{H up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~I::
ControlSend,,{I down}{I up}, ahk_id %windowid1%
ControlSend,,{I down}{I up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^I::
ControlSend,,{Ctrl down}{I down}{I up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{I down}{I up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!I::
ControlSend,,{Alt down}{I down}{I up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{I down}{I up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+I::
ControlSend,,{Shift down}{I down}{I up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{I down}{I up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~J::
ControlSend,,{J down}{J up}, ahk_id %windowid1%
ControlSend,,{J down}{J up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^J::
ControlSend,,{Ctrl down}{J down}{J up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{J down}{J up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!J::
ControlSend,,{Alt down}{J down}{J up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{J down}{J up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+J::
ControlSend,,{Shift down}{J down}{J up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{J down}{J up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~K::
ControlSend,,{K down}{K up}, ahk_id %windowid1%
ControlSend,,{K down}{K up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^K::
ControlSend,,{Ctrl down}{K down}{K up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{K down}{K up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!K::
ControlSend,,{Alt down}{K down}{K up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{K down}{K up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+K::
ControlSend,,{Shift down}{K down}{K up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{K down}{K up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~L::
ControlSend,,{L down}{L up}, ahk_id %windowid1%
ControlSend,,{L down}{L up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^L::
ControlSend,,{Ctrl down}{L down}{L up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{L down}{L up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!L::
ControlSend,,{Alt down}{L down}{L up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{L down}{L up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+L::
ControlSend,,{Shift down}{L down}{L up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{L down}{L up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~M::
ControlSend,,{M down}{M up}, ahk_id %windowid1%
ControlSend,,{M down}{M up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^M::
ControlSend,,{Ctrl down}{M down}{M up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{M down}{M up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!M::
ControlSend,,{Alt down}{M down}{M up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{M down}{M up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+M::
ControlSend,,{Shift down}{M down}{M up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{M down}{M up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~N::
ControlSend,,{N down}{N up}, ahk_id %windowid1%
ControlSend,,{N down}{N up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^N::
ControlSend,,{Ctrl down}{N down}{N up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{N down}{N up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!N::
ControlSend,,{Alt down}{N down}{N up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{N down}{N up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+N::
ControlSend,,{Shift down}{N down}{N up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{N down}{N up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~O::
ControlSend,,{O down}{O up}, ahk_id %windowid1%
ControlSend,,{O down}{O up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^O::
ControlSend,,{Ctrl down}{O down}{O up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{O down}{O up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!O::
ControlSend,,{Alt down}{O down}{O up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{O down}{O up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+O::
ControlSend,,{Shift down}{O down}{O up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{O down}{O up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~P::
ControlSend,,{P down}{P up}, ahk_id %windowid1%
ControlSend,,{P down}{P up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^P::
ControlSend,,{Ctrl down}{P down}{P up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{P down}{P up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!P::
ControlSend,,{Alt down}{P down}{P up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{P down}{P up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+P::
ControlSend,,{Shift down}{P down}{P up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{P down}{P up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~Q::
ControlSend,,{Q down}{Q up}, ahk_id %windowid1%
ControlSend,,{Q down}{Q up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^Q::
ControlSend,,{Ctrl down}{Q down}{Q up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{Q down}{Q up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!Q::
ControlSend,,{Alt down}{Q down}{Q up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{Q down}{Q up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+Q::
ControlSend,,{Shift down}{Q down}{Q up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{Q down}{Q up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~R::
ControlSend,,{R down}{R up}, ahk_id %windowid1%
ControlSend,,{R down}{R up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^R::
ControlSend,,{Ctrl down}{R down}{R up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{R down}{R up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!R::
ControlSend,,{Alt down}{R down}{R up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{R down}{R up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+R::
ControlSend,,{Shift down}{R down}{R up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{R down}{R up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~T::
ControlSend,,{T down}{T up}, ahk_id %windowid1%
ControlSend,,{T down}{T up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^T::
ControlSend,,{Ctrl down}{T down}{T up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{T down}{T up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!T::
ControlSend,,{Alt down}{T down}{T up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{T down}{T up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+T::
ControlSend,,{Shift down}{T down}{T up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{T down}{T up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~U::
ControlSend,,{U down}{U up}, ahk_id %windowid1%
ControlSend,,{U down}{U up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^U::
ControlSend,,{Ctrl down}{U down}{U up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{U down}{U up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!U::
ControlSend,,{Alt down}{U down}{U up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{U down}{U up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+U::
ControlSend,,{Shift down}{U down}{U up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{U down}{U up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~V::
ControlSend,,{V down}{V up}, ahk_id %windowid1%
ControlSend,,{V down}{V up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^V::
ControlSend,,{Ctrl down}{V down}{V up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{V down}{V up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!V::
ControlSend,,{Alt down}{V down}{V up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{V down}{V up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+V::
ControlSend,,{Shift down}{V down}{V up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{V down}{V up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~X::
ControlSend,,{X down}{X up}, ahk_id %windowid1%
ControlSend,,{X down}{X up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^X::
ControlSend,,{Ctrl down}{X down}{X up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{X down}{X up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!X::
ControlSend,,{Alt down}{X down}{X up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{X down}{X up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+X::
ControlSend,,{Shift down}{X down}{X up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{X down}{X up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~Y::
ControlSend,,{Y down}{Y up}, ahk_id %windowid1%
ControlSend,,{Y down}{Y up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^Y::
ControlSend,,{Ctrl down}{Y down}{Y up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{Y down}{Y up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!Y::
ControlSend,,{Alt down}{Y down}{Y up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{Y down}{Y up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+Y::
ControlSend,,{Shift down}{Y down}{Y up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{Y down}{Y up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~Z::
ControlSend,,{Z down}{Z up}, ahk_id %windowid1%
ControlSend,,{Z down}{Z up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^Z::
ControlSend,,{Ctrl down}{Z down}{Z up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{Z down}{Z up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!Z::
ControlSend,,{Alt down}{Z down}{Z up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{Z down}{Z up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+Z::
ControlSend,,{Shift down}{Z down}{Z up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{Z down}{Z up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~<::
ControlSend,,{< down}{< up}, ahk_id %windowid1%
ControlSend,,{< down}{< up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^<::
ControlSend,,{Ctrl down}{< down}{< up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{< down}{< up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!<::
ControlSend,,{Alt down}{< down}{< up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{< down}{< up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+<::
ControlSend,,{Shift down}{< down}{< up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{< down}{< up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~/::
ControlSend,,{/ down}{/ up}, ahk_id %windowid1%
ControlSend,,{/ down}{/ up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^/::
ControlSend,,{Ctrl down}{/ down}{/ up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{/ down}{/ up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!/::
ControlSend,,{Alt down}{/ down}{/ up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{/ down}{/ up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+/::
ControlSend,,{Shift down}{/ down}{/ up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{/ down}{/ up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~\::
ControlSend,,{\ down}{\ up}, ahk_id %windowid1%
ControlSend,,{\ down}{\ up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^\::
ControlSend,,{Ctrl down}{\ down}{\ up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{\ down}{\ up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!\::
ControlSend,,{Alt down}{\ down}{\ up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{\ down}{\ up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+\::
ControlSend,,{Shift down}{\ down}{\ up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{\ down}{\ up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+::
ControlSend,,{+ down}{+ up}, ahk_id %windowid1%
ControlSend,,{+ down}{+ up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^+::
ControlSend,,{Ctrl down}{+ down}{+ up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{+ down}{+ up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!+::
ControlSend,,{Alt down}{+ down}{+ up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{+ down}{+ up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~++::
ControlSend,,{Shift down}{+ down}{+ up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{+ down}{+ up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~*::
ControlSend,,{* down}{* up}, ahk_id %windowid1%
ControlSend,,{* down}{* up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^*::
ControlSend,,{Ctrl down}{* down}{* up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{* down}{* up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!*::
ControlSend,,{Alt down}{* down}{* up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{* down}{* up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+*::
ControlSend,,{Shift down}{* down}{* up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{* down}{* up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~-::
ControlSend,,{- down}{- up}, ahk_id %windowid1%
ControlSend,,{- down}{- up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^-::
ControlSend,,{Ctrl down}{- down}{- up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{- down}{- up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!-::
ControlSend,,{Alt down}{- down}{- up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{- down}{- up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+-::
ControlSend,,{Shift down}{- down}{- up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{- down}{- up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~sc029::
ControlSend,,{sc029 down}{sc029 up}, ahk_id %windowid1%
ControlSend,,{sc029 down}{sc029 up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^sc029::
ControlSend,,{Ctrl down}{sc029 down}{sc029 up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{sc029 down}{sc029 up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!sc029::
ControlSend,,{Alt down}{sc029 down}{sc029 up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{sc029 down}{sc029 up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+sc029::
ControlSend,,{Shift down}{sc029 down}{sc029 up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{sc029 down}{sc029 up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~TAB::
ControlSend,,{TAB down}{TAB up}, ahk_id %windowid1%
ControlSend,,{TAB down}{TAB up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^TAB::
ControlSend,,{Ctrl down}{TAB down}{TAB up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{TAB down}{TAB up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+TAB::
ControlSend,,{Shift down}{TAB down}{TAB up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{TAB down}{TAB up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~ESC::
ControlSend,,{ESC down}{ESC up}, ahk_id %windowid1%
ControlSend,,{ESC down}{ESC up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~^ESC::
ControlSend,,{Ctrl down}{ESC down}{ESC up}{Ctrl up}, ahk_id %windowid1%
ControlSend,,{Ctrl down}{ESC down}{ESC up}{Ctrl up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~!ESC::
ControlSend,,{Alt down}{ESC down}{ESC up}{Alt up}, ahk_id %windowid1%
ControlSend,,{Alt down}{ESC down}{ESC up}{Alt up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
~+ESC::
ControlSend,,{Shift down}{ESC down}{ESC up}{Shift up}, ahk_id %windowid1%
ControlSend,,{Shift down}{ESC down}{ESC up}{Shift up}, ahk_id %windowid2%
return

#IfWinActive, World of Warcraft
SPACE::
ControlSend,,{SPACE down}{SPACE up}, ahk_id %windowid1%
ControlSend,,{SPACE down}{SPACE up}, ahk_id %windowid2%
return
