# Multibox

This is a AHK script generator. 

# Usage
Requirements: Windows 10 (untested with older versions).

This script is configured 5 "World of Wacraft" windows and for the "A,S,D,W"
movement keys. 

1. Run all your "World of Warcraft" instances.

2. Execute the `script.exe`.

NOTE: make sure to open the windows of ""orld of Warcraft" before running the
script. 

# Usefull tips

## Addons
- EMA (optional): [Classic](https://www.curseforge.com/wow/addons/ema-classic) /
    [Retail](https://www.curseforge.com/wow/addons/ema).  
    This addon is great to configure your team (auto accept and share
    quests, automatically set the leader, and much more).
- Azeroth Auto Pilot (optionnal):
    [Classic](https://www.curseforge.com/wow/addons/azeroth-auto-pilot-classic)
    [Retail](https://www.curseforge.com/wow/addons/azeroth-auto-pilot)  
    This addon tells you what quest and where to go to level up very fast.

## Macros and settings

In EMA configuration panel (`/ema config`). I recommend to 
- Team > Automatically set the loot method to `Free for all`
- Toons > Follow: enable the autofollow and add a delay of 4 seconds. Therefore, you
  will have the time too loot with your alts (check bellow).

There are a lot of options in EMA. I really recommend you to take the time
testing them and configure it as you which.

The only thing that is missing in EMA, is a proper way to make all your alts
looting or interacting with a NPC.

### How to make your alt loot or interact with a NPC

TODO: use the `Interact with target` in the WoW Bindings. Create a macro to
focus the leader target. 

### Macros and recommendation

TODO: define leader, /assist, /follow.

# Custom settings and build

The script is configured to ignore 'A,S,D,W' as movement keys. In case this is
not the case for you, you will need to rebuild the script with your own keys.
Here are the step to do so.

Requirements: 
- Windows 10 (untested with older versions)
- [NodeJS v14.8.0](https://nodejs.org/en/download/) - [Download
    Installer](https://nodejs.org/dist/v15.0.1/node-v15.0.1-x64.msi)
- [AHK v2](https://www.autohotkey.com/v2/) - [Download
    Installer](https://www.autohotkey.com/download/ahk-install.exe)
- [Git](https://gitforwindows.org/) - [Download
    Installer](https://github.com/git-for-windows/git/releases/download/v2.29.1.windows.1/Git-2.29.1-64-bit.exe)
- A bit of coding skill.
  
Open you terminal and run the following commands:

```bash
git clone https://gitlab.com/yveslange.public/gaming/multibox
cd multibox
npm install
cp config.sample.js config.js
```

## Configure

Now you are ready to configure the application as you wish. Meaning that you are
able to bind or ignore some custom keys.

Open the `config.js` to define your game window name, the number of running
instance and the keys to broadcast or ignore.

You should update the `ignoredKeys` field if your movement keys are not 'ASDW'.


## Build the AHK script

Run the following command in your terminal:
```bash
npm start
```

This will build the `script.ahk` (note that it is not the executable `script.exe`).

You can execute the `script.ahk` by double clicking on it. At this point,
everything should work fine with you own keys.

## Build the executable file (.exe)
- Requirement: AHK should be installed in the `C:\Program Files\AutoHotkey`
    folder on your computer.

Run the following command in your terminal:
```
npm run build
```

This will generate the executable `build/script.exe`. You can now use the
executable file.
